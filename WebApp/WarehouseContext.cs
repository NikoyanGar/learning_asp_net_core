﻿using System.Linq;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using WebApp.Models;
using UserStore = WebApp.Models.UserStore;

namespace WebApp
{
    public class WarehouseContext : IdentityDbContext<User, Role, int>
    {
        public WarehouseContext(DbContextOptions<WarehouseContext> options) : base(options)
        {

        }
        #region Dbset
        public DbSet<Email> Emails { get; set; }
        public DbSet<Geoposition> Geopositions { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<MeasurementUnit> MeasurementUnits { get; set; }
        public DbSet<Person> Persons { get; set; }
        public DbSet<PersonalInformation> PersonalInformations { get; set; }
        public DbSet<PersonPersonType> PersonsPersonTypes { get; set; }
        public DbSet<PersonType> PersonTypes { get; set; }
        public DbSet<PhoneNumber> PhoneNumbers { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<DetailedInfo> DetailedInfos { get; set; }
        public DbSet<ImagePath> ImagePaths { get; set; }
        public DbSet<Purchase> Purchases { get; set; }
        public DbSet<PurchaseProduct> PurchaseProducts { get; set; }
        public DbSet<Sale> Sales { get; set; }
        public DbSet<SaleProduct> SaleProducts { get; set; }
        public DbSet<Store> Stores { get; set; }
        public DbSet<StoreProduct> StoreProducts { get; set; }
        public DbSet<StoreStoreType> StoresStoreTypes { get; set; }
        public DbSet<StoreType> StoreTypes { get; set; }
        public DbSet<UserStorage> UsersStorages { get; set; }
        public DbSet<UserStore> UsersStores { get; set; }
        #endregion

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<StoreType>().HasData(new StoreType[]
            {
                new StoreType { Id = 1, Value = "Store" },
                new StoreType { Id = 2, Value = "Storage" }
            });

            modelBuilder.Entity<PersonType>().HasData(new PersonType[]
            {
                new PersonType { Id = 1, Value = "Provider" },
                new PersonType { Id = 2, Value = "Customer" }
            });


            modelBuilder.Entity<UserStorage>().HasOne(r => r.User).WithMany(a => a.UserStorages).HasForeignKey(k => k.UserId);

            modelBuilder.Entity<UserStore>().HasOne(r => r.User).WithMany(r => r.UserStores).HasForeignKey(k => k.UserId);

            var cascadeFKs = modelBuilder.Model.GetEntityTypes()
                    .SelectMany(t => t.GetForeignKeys())
                    .Where(fk => !fk.IsOwnership && fk.DeleteBehavior == DeleteBehavior.Cascade);

            foreach (var fk in cascadeFKs)
                fk.DeleteBehavior = DeleteBehavior.Restrict;

            base.OnModelCreating(modelBuilder);
        }

        
    }
}
