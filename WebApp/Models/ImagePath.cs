﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class ImagePath
    {
        public int Id { get; set; }
        public string Path { get; set; }

        public DetailedInfo DetailedInfo { get; set; }
        public int DetailedInfoId { get; set; }
    }
}
