﻿using System;

using System.ComponentModel.DataAnnotations.Schema;

using WebApp.Models.Interfaces;

namespace WebApp.Models
{
    public class Group : IUserDateStamp
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }

        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
        public DateTimeOffset UpdatedDate { get; set; }
        [ForeignKey("CreatedBy")]
        public User CreatedUser { get; set; }
        [ForeignKey("UpdatedBy")]
        public User UpdatedUser { get; set; }
    }
}
