﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class User : IdentityUser<int>
    {
        public int? PersonalInformationId { get; set; }

        public PersonalInformation PersonalInfrormation { get; set; }
        public ICollection<UserStore> UserStores { get; set; }
        public ICollection<UserStorage> UserStorages { get; set; }
    }
}
