﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Models.Interfaces;

namespace WebApp.Models
{
    public class Product : IUserDateStamp
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Characteristic { get; set; }
        public int GroupId { get; set; }
        public int UnitId { get; set; }

        public Group Group { get; set; }
        public MeasurementUnit Unit { get; set; }

        public int DetailedInfoId { get; set; }
        public DetailedInfo DetailedInfo { get; set; }

        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
        public DateTimeOffset UpdatedDate { get; set; }
        [ForeignKey("CreatedBy")]
        public User CreatedUser { get; set; }
        [ForeignKey("UpdatedBy")]
        public User UpdatedUser { get; set; }
    }
}
