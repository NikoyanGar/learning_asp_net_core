﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Models.Interfaces;

namespace WebApp.Models
{
    public class Email : IIdValue<int, string>
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public int PersonalInformationId { get; set; }

        public PersonalInformation PersonInformation { get; set; }
    }
}
