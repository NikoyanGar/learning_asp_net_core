﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class Purchase

    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int PersonId { get; set; }
        public int UserId { get; set; }
        public DateTimeOffset Date { get; set; }

        public Person Person { get; set; }
        public User User { get; set; }
    }
}
