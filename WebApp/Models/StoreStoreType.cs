﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class StoreStoreType
    {
        public int Id { get; set; }
        public int StoreId { get; set; }
        public int StoreTypeId { get; set; }

        public Store Store { get; set; }
        public StoreType StoreType { get; set; }
    }
}
