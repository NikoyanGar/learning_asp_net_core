﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class UserStorage
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int StorageId { get; set; }

        public User User { get; set; }
        public Store Storage { get; set; }
    }
}
