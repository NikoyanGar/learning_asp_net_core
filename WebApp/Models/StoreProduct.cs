﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Models.Interfaces;

namespace WebApp.Models
{
    public class StoreProduct : IUserDateStamp
    {
        public int Id { get; set; }
        public int StoreId { get; set; }
        public int ProductId { get; set; }
        public double Count { get; set; }

        public Store Store { get; set; }
        public Product Product { get; set; }

        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
        public DateTimeOffset UpdatedDate { get; set; }
        [ForeignKey("CreatedBy")]
        public User CreatedUser { get; set; }
        [ForeignKey("UpdatedBy")]
        public User UpdatedUser { get; set; }
    }
}
