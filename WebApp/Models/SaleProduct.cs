﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class SaleProduct
    {
        public int Id { get; set; }
        public int SaleId { get; set; }
        public int ProductId { get; set; }
        public int StorageProductId { get; set; }
        public double Count { get; set; }
        public double Price { get; set; }
        public double Discount { get; set; }

        public Sale Sale { get; set; }
        public StoreProduct StorageProduct { get; set; }
    }
}
