﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class Person
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int? PersonalInformationId { get; set; }

        public PersonalInformation PersonalInformation { get; set; }
    }
}
