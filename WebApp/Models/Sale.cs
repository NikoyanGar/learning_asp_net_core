﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class Sale
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int PersonId { get; set; }
        public int UserId { get; set; }
        public int StoreId { get; set; }
        public double Discount { get; set; }
        public bool Wholesale { get; set; }
        public DateTimeOffset Date { get; set; }

        public Person Person { get; set; }
        public User User { get; set; }
        public Store Store { get; set; }
    }
}
