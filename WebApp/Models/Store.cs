﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Models.Interfaces;

namespace WebApp.Models
{
    public class Store : IUserDateStamp
    {
        public int Id { get; set; }
        public string Name { get; set; }
      

        public int GeopositionId { get; set; }
        public Geoposition Geoposition { get; set; }

        public ICollection<UserStore> UserStores { get; set; }
        public ICollection<UserStorage> UserStorages { get; set; }

        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
        public DateTimeOffset UpdatedDate { get; set; }
        [ForeignKey("CreatedBy")]
        public User CreatedUser { get; set; }
        [ForeignKey("UpdatedBy")]
        public User UpdatedUser { get; set; }
    }
}
