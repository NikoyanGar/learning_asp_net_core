﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class DetailedInfo
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string AdditionalCharacteristics { get; set; }
        public string MainImagePath { get; set; }

        public Product Product { get; set; }
        public List<ImagePath> ImagePaths { get; set; }
       

    }
}
