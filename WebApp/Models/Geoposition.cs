﻿using System;
using System.Collections.Generic;
using WebApp.Models.Interfaces;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApp.Models
{
    public class Geoposition : IUserDateStamp
    {
        public int Id { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public string Description { get; set; }

        public int StoreId { get; set; }


        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
        public DateTimeOffset UpdatedDate { get; set; }
        [ForeignKey("CreatedBy")]
        public User CreatedUser { get; set; }
        [ForeignKey("UpdatedBy")]
        public User UpdatedUser { get; set; }
    }
}
