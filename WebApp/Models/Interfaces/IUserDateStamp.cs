﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models.Interfaces
{
    interface IUserDateStamp
    {
        int CreatedBy { get; set; }
        int UpdatedBy { get; set; }
        DateTimeOffset CreatedDate { get; set; }
        DateTimeOffset UpdatedDate { get; set; }

        [ForeignKey("CreatedBy")]
        User CreatedUser { get; set; }
        [ForeignKey("UpdatedBy")]
        User UpdatedUser { get; set; }
    }
}
