﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models.Interfaces
{
    public interface IIdValue<TId, TValue>
    {
        TId Id { get; set; }
        TValue Value { get; set; }
    }
}
