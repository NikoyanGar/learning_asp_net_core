﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class PersonPersonType
    {
        public int Id { get; set; }
        public int PersonId { get; set; }
        public int PersonTypeId { get; set; }

        public Person Person { get; set; }
        public PersonType PersonType { get; set; }
    }
}
