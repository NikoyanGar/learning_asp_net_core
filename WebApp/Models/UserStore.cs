﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class UserStore
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int StoreId { get; set; }

        public User User { get; set; }
        public Store Store { get; set; }
    }
}
