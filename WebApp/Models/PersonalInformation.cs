﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class PersonalInformation
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Description { get; set; }

        public ICollection<Email> Emails { get; set; }
        public ICollection<PhoneNumber> PhoneNumbers { get; set; }
    }
}
