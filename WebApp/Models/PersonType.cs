﻿using WebApp.Models.Interfaces;

namespace WebApp.Models
{
    public class PersonType : IIdValue<int, string>
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
