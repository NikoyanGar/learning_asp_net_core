﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class PurchaseProduct
    {
        public int Id { get; set; }
        public int PurchaseId { get; set; }
        public int StoreProductId { get; set; }
        public double Count { get; set; }
        public double Price { get; set; }

        public Purchase Purchase { get; set; }
        public StoreProduct StoreProduct { get; set; }
    }
}
