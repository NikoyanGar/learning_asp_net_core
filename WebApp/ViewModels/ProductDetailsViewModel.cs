﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.ViewModels
{
    public class ProductDetailsViewModel
    {
        public int ProductId { get; set; }
        public string Productname { get; set; }
        public string Productcode { get; set; }
        public string Description { get; set; }
        public string Characteristic { get; set; }
        public string AdditionalCharacteristics { get; set; }
        public string MainImagePath { get; set; }
        public List<string> Paths { get; set; }
        public int GroupId { get; set; }
        public string Groupname { get; set; }

    }
}
