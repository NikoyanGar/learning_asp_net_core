﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Models;

namespace WebApp.ViewModels
{
    public class ProductCreateViewModel
    {
        public List<Group> Groups { get; set; }
        public List<Store> Stores { get; set; }

    }
}
