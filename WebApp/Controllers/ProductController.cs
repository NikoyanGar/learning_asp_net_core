﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApp.Models;
using WebApp.ViewModels;

namespace WebApp.Controllers
{
    public class ProductController : Controller
    {

        private readonly WarehouseContext _context;

        public ProductController(WarehouseContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var grups = await _context.Groups.ToListAsync();
            return View(grups);
        }
        
        [HttpGet]
        public async Task<IActionResult> ViewProducts(int id)
        {
            var products = await _context.Products.Where(p => p.GroupId == id).ToListAsync();
            return View(products);
        }

        [HttpGet]
        public async Task<IActionResult> Details(int id)
        {
            var productDetailsViewModel = await _context.Products.Where(p => p.Id == id)
                .Select(p => new ProductDetailsViewModel
                {
                    AdditionalCharacteristics = p.DetailedInfo.AdditionalCharacteristics,
                    Characteristic = p.Characteristic,
                    Description = p.DetailedInfo.Description,
                    GroupId = p.GroupId,
                    Groupname = p.Group.Name,
                    MainImagePath = p.DetailedInfo.MainImagePath,
                    Productcode = p.Code,
                    ProductId = p.Id,
                    Productname = p.Name,
                    Paths = p.DetailedInfo.ImagePaths.Select(im => im.Path).ToList()
                })
                .FirstOrDefaultAsync();

            return View(productDetailsViewModel);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }
    }
}