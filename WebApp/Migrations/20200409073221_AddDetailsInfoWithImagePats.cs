﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApp.Migrations
{
    public partial class AddDetailsInfoWithImagePats : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DetailedInfoId",
                table: "Products",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "DetailedInfos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Details = table.Column<string>(nullable: true),
                    MainImagePath = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DetailedInfos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ImagePaths",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Path = table.Column<string>(nullable: true),
                    DetailedInfoId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ImagePaths", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ImagePaths_DetailedInfos_DetailedInfoId",
                        column: x => x.DetailedInfoId,
                        principalTable: "DetailedInfos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Products_DetailedInfoId",
                table: "Products",
                column: "DetailedInfoId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ImagePaths_DetailedInfoId",
                table: "ImagePaths",
                column: "DetailedInfoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_DetailedInfos_DetailedInfoId",
                table: "Products",
                column: "DetailedInfoId",
                principalTable: "DetailedInfos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_DetailedInfos_DetailedInfoId",
                table: "Products");

            migrationBuilder.DropTable(
                name: "ImagePaths");

            migrationBuilder.DropTable(
                name: "DetailedInfos");

            migrationBuilder.DropIndex(
                name: "IX_Products_DetailedInfoId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "DetailedInfoId",
                table: "Products");
        }
    }
}
