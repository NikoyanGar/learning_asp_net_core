﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApp.Migrations
{
    public partial class Codereview : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Stores_Geopositions_GeopositionId",
                table: "Stores");

            migrationBuilder.DropIndex(
                name: "IX_Stores_GeopositionId",
                table: "Stores");

            migrationBuilder.AlterColumn<int>(
                name: "GeopositionId",
                table: "Stores",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "GeopositionId1",
                table: "Stores",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StoreId",
                table: "Geopositions",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Stores_GeopositionId1",
                table: "Stores",
                column: "GeopositionId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Stores_Geopositions_GeopositionId1",
                table: "Stores",
                column: "GeopositionId1",
                principalTable: "Geopositions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Stores_Geopositions_GeopositionId1",
                table: "Stores");

            migrationBuilder.DropIndex(
                name: "IX_Stores_GeopositionId1",
                table: "Stores");

            migrationBuilder.DropColumn(
                name: "GeopositionId1",
                table: "Stores");

            migrationBuilder.DropColumn(
                name: "StoreId",
                table: "Geopositions");

            migrationBuilder.AlterColumn<int>(
                name: "GeopositionId",
                table: "Stores",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.CreateIndex(
                name: "IX_Stores_GeopositionId",
                table: "Stores",
                column: "GeopositionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Stores_Geopositions_GeopositionId",
                table: "Stores",
                column: "GeopositionId",
                principalTable: "Geopositions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
