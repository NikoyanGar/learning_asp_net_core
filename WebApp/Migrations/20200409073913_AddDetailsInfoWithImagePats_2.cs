﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApp.Migrations
{
    public partial class AddDetailsInfoWithImagePats_2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Details",
                table: "DetailedInfos");

            migrationBuilder.AddColumn<string>(
                name: "AdditionalCharacteristics",
                table: "DetailedInfos",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "DetailedInfos",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AdditionalCharacteristics",
                table: "DetailedInfos");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "DetailedInfos");

            migrationBuilder.AddColumn<string>(
                name: "Details",
                table: "DetailedInfos",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
